This repository has Ansible playbooks to manage the configuration of
KDE's server infrastructure.

Currently it's only managing two things: backup scripts,
and git-daemon configuration for the two anongit servers.

Sensitive information (passwords, etc) is encrypted using ansible-vault;
the vault password is only known to KDE sysadmins.

Example for how to refresh the backup configuration:

```bash
ansible-playbook -D -i production backups.yml
```

The `-D` is optional, it will show diffs for exactly what was changed.
You can also add `-C` to do a "dry run" without modifying anything,
only showing what changes would be done.
