Configure mirrorbrain for download.kde.org and files.kde.org.

NOTE: this role is not enough to replicate the services, as it's missing more
files. It's only enough to maintain the existing configuration in version
control.
