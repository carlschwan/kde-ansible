		</div> <!-- /pageRow -->

		<div id="footerRow" class="container small align-center">
			<hr />

			<footer>
				<p>Powered by <a href="http://mirrorbrain.org/">MirrorBrain</a> - Maintained by
				<a href="https://bugs.kde.org/enter_sysadmin_request.cgi">KDE Sysadmin</a> -
				<a href="http://www.kde.org/mirrors/ftp_howto.php">Want to be a mirror?</a><br />
				KDE<sup>&#174;</sup> and <a href="http://www.kde.org/media/images/trademark_kde_gear_black_logo.png">
				the K Desktop Environment<sup>&#174;</sup> logo</a> are registered trademarks of
				<a href="http://ev.kde.org/" title="Homepage of the KDE non-profit Organization">KDE e.V.</a> |
				<a href="http://www.kde.org/community/whatiskde/impressum.php">Legal</a></p>
			</footer>
		</div>

		<script type="text/javascript" src="http://cdn.kde.org/js/jquery.js"></script>
		<script type="text/javascript" src="http://cdn.kde.org/js/min/bootstrap.js"></script>
		<script type="text/javascript" src="http://cdn.kde.org/js/min/bootstrap-neverland.js"></script>
		<script type="text/javascript" src="/extra/javascript.js"></script>
	</body>
</html>