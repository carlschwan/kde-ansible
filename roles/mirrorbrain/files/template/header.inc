<!DOCTYPE html>
<html>
	<head>
		<title>KDE - Experience Freedom!</title>

		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="description" content="KDE Homepage, KDE.org">

		<link rel="shortcut icon" href="http://cdn.kde.org/img/favicon.ico">
		<link rel="stylesheet" media="screen" type="text/css" href="http://cdn.kde.org/css/bootstrap.css">
		<link rel="stylesheet" media="screen" type="text/css" href="http://cdn.kde.org/css/bootstrap-responsive.css">
		<link rel="stylesheet" media="screen" type="text/css" href="http://cdn.kde.org/css/bootstrap-download.css">
	</head>

	<body>
		<div class="navbar navbar-static-top Neverland">
			<div class="navbar-inner">
				<div class="container">
					<div class="nav-collapse">
						<ul class="nav pull-right">
							<li>
								<a href="http://download.kde.org/extra/mirrors.html">
									<i class="icon-download"></i>
									Official KDE Mirrors
								</a>
							</li>
							<li>
								<a href="http://download.kde.org/extra/files-mirrors.html">
									<i class="icon-list"></i>
									KDE Application Data Mirrors
								</a>
							</li>
						</ul>
					</div>

					<a href="http://kde.org" class="brand">
						<img src="http://cdn.kde.org/img/logo.plain.small.png" id="site_logo" alt="" />
						KDE
					</a>
				</div>
			</div>
		</div>

		<div id="pageRow" class="container">
			<a href="http://jointhegame.kde.org" class="thumbnail">
				<div class="global-banner"></div>
			</a>