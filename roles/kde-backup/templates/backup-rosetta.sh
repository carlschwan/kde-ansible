LOCATION=/root/{{backup_directory}}

# Backup databases
for DATABASE in `mysqlshow | grep -v "information_schema" | grep -v "performance_schema" | grep -v "mysql" | tail -n+4 | awk '{print $2}'`; do
    mysqldump --opt --quick --single-transaction --skip-extended-insert  --events --create-options --set-charset  $DATABASE > $LOCATION/db.$DATABASE.`date +%w`.sql
    xz -f $LOCATION/db.$DATABASE.`date +%w`.sql
done

# Backup homes!
tar -cJf $LOCATION/homes.`date +%w`.tar.xz --exclude=home/scripty/prod -C / home/

# Backup application data
tar -cJf $LOCATION/srv.`date +%w`.tar.xz -C / --exclude=srv/l10n-svn srv/ 2>&1 | grep -v "file changed as we read it"

# Backup configuration and list of installed packages
tar -czf $LOCATION/etc.`date +%w`.tgz -C / etc/
tar -czf $LOCATION/crontabs.`date +%w`.tgz -C / var/spool/cron/
dpkg -l > $LOCATION/dpkg.`date +%w`

# Secure the backups
chmod -R 700 $LOCATION

# Transfer them to the backup server
cd $LOCATION/..
rsync --timeout=600 --delete -a {{backup_directory}} -4 {{gohma_backup_user}}@micrea.kde.org:
