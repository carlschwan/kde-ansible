#!/bin/bash
LOCATION=/root/{{backup_directory}}

# Grab our databases
for DATABASE in `mysqlshow | grep -v "information_schema" | grep -v "performance_schema" | grep -v "mysql" | tail -n+4 | awk '{print $2}'`; do
  mysqldump --skip-extended-insert $DATABASE | xz - > $LOCATION/db.$DATABASE.`date +%w`.sql.xz
done

# Backup the mailman install and some other tools
tar -cJf $LOCATION/mailman-install.`date +%w`.tar.xz -C / usr/lib/mailman
tar -cJf $LOCATION/root-scripts.`date +%w`.tar.xz -C / root/bin
tar -cJf $LOCATION/commitfilter.`date +%w`.tar.xz -C / --exclude=home/commitfilter/procmail.log home/commitfilter

# Backup the system configuration, crontabs and package listing
tar -czf $LOCATION/etc.`date +%w`.tgz -C / etc/
tar -czf $LOCATION/crontabs.`date +%w`.tgz -C / var/spool/cron/
dpkg -l > $LOCATION/dpkg.`date +%w`

# Transfer traditional backups to the backup server
cd $LOCATION/..
rsync --timeout=600 --delete -a {{backup_directory}} -4 {{gohma_backup_user}}@micrea.kde.org:backups/archives/

# Prepare to run Borg backups
export BORG_PASSPHRASE='{{backup_borg_passphrase}}'
export BORG_SERVER="{{gohma_backup_user}}@micrea.kde.org"

# Backup Mailman archives and lists
export BORG_REPO="$BORG_SERVER:backups/borg/mailman"
borg create --compression zlib,5 --exclude-caches ::'{hostname}-mailman-{now}' /mnt/data/mailman/
borg prune --prefix '{hostname}-' --keep-daily 7 --keep-weekly 4 --keep-monthly 6 2>&1 | grep -v "Compacting segments"
