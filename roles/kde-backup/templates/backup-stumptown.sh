#!/bin/bash
LOCATION=/root/{{backup_directory}}

# Cleanup old backups
find $LOCATION -mtime +21 | xargs rm -f

# Determine the machine hosting our databases
SERVER=`cat ~/db.passwds | grep server | grep -v "kde_dot" | awk '{print $2}'`

# Backup the databases
IFS="|"
for original in `grep "^kde" ~/db.passwds | tr -s " " | tr -d "\n"` ; do
	DB=${original%% *}; rest=${original#* }
	USER=${rest%% *}; rest=${rest#* }
	PASS=${rest%% *};
	mysqldump --skip-extended-insert -h$SERVER -u$USER -p$PASS $DB > $LOCATION/db.$DB.`date +%W`.sql;
	xz -f $LOCATION/db.$DB.`date +%W`.sql;
done

# Grab all of the web applications we run
rm $LOCATION/srv-www.tar.xz
tar -cJf $LOCATION/srv-www.tar.xz -C / srv

# Grab the system configuration, package listing and cronjobs
tar -cf $LOCATION/etc.`date +%W`.tar -C / etc/
tar -czf $LOCATION/crontabs.`date +%W`.tgz -C / var/spool/cron/
dpkg -l > $LOCATION/dpkg.`date +%W`

# Secure our backups
chmod -R 700 $LOCATION

# Transfer them to our backup server
cd $LOCATION/..
rsync --timeout=600 --delete -a {{backup_directory}} {{gohma_backup_user}}@micrea.kde.org:
