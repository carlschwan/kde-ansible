#!/bin/bash
LOCATION=/root/{{backup_directory}}

# Cleanup old backups
find $LOCATION -name '*z' -mtime +21 | xargs rm -f

# Grab the system config, package listing and cronjobs
tar -czf $LOCATION/crontabs.`date +%w`.tgz -C / var/spool/cron/
tar -czf $LOCATION/etc.`date +%w`.tgz -C / etc/
dpkg -l > $LOCATION/dpkg.`date +%w`

# Grab Grafana's SQLite DB
cp /var/lib/grafana/grafana.db $LOCATION/grafana.db.`date +%w`

# Secure our backups
chmod -R 700 $LOCATION

# Transfer backups to backup server
cd $LOCATION/..
rsync --timeout=600 --delete -a {{backup_directory}}/ -4 {{gohma_backup_user}}@micrea.kde.org:backups/archives/

# Prepare to run Borg backups
export BORG_PASSPHRASE='{{backup_borg_passphrase}}'
BORG_SERVER="ssh://{{gohma_backup_user}}@micrea.kde.org"

# Backup databases
export BORG_REPO="$BORG_SERVER/./backups/borg/entypes-databases"
for DATABASE in telemetry stats; do
    mysqldump --opt --quick --single-transaction --skip-extended-insert --events --create-options --set-charset $DATABASE | borg create --compression lzma,3 "::{hostname}-$DATABASE-{now}" - 2>&1 | grep -v "Compacting segments"
    borg prune --prefix "{hostname}-$DATABASE" --keep-daily 7 --keep-weekly 4 --keep-monthly 3 2>&1 | grep -v "Compacting segments"
done
