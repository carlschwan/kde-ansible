LOCATION=/root/{{backup_directory}}

# Cleanup old backups
find $LOCATION/ -name '*gz' -mtime +7 | xargs rm -f

# Grab the system configuration, package listing, and cronjobs
dpkg -l > $LOCATION/dpkg.`date +%w`
tar -czf $LOCATION/crontabs.`date +%w`.tgz -C / var/spool/cron/
tar -czf $LOCATION/etc.`date +%w`.tgz -C / etc/

# Secure our backups
chmod -R 700 $LOCATION

# Transfer them to the backup server
cd $LOCATION/..
rsync --timeout=600 --delete -a {{backup_directory}} -4 {{gohma_backup_user}}@micrea.kde.org:
