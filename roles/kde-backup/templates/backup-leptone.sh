#!/bin/bash
LOCATION=/root/{{backup_directory}}
find $LOCATION -name '*.tar' -mtime +7 | xargs rm -f

# Backup configuration and list of installed packages
tar -czf $LOCATION/etc.`date +%w`.tgz -C / etc/
tar -czf $LOCATION/crontabs.`date +%w`.tgz -C / var/spool/cron/
dpkg -l > $LOCATION/dpkg.`date +%w`

# Transfer the backups to the backup server
lftp -f ~/bin/backup-options

# Prepare to run Borg backups
export BORG_PASSPHRASE='{{backup_borg_passphrase}}'

{% set backup_creds = hetzner_backup_creds[hetzner_backup_host] %}
BORG_SERVER="ssh://{{backup_creds.username}}@{{backup_creds.hostname}}:23"

# Backup Homes
export BORG_REPO="$BORG_SERVER/./borg-backups/homes"
borg create --compression lzma,3 --exclude-caches --exclude /home/git/gitlab/tmp/ ::'{hostname}-homes-{now}' /home/ 2>&1 | grep -v "Compacting segments"
borg prune --prefix '{hostname}-' --keep-daily 14 --keep-weekly 8 --keep-monthly 6 2>&1 | grep -v "Compacting segments"

# Backup Git Repositories
export BORG_REPO="$BORG_SERVER/./borg-backups/git-repositories"
borg create --compression none --exclude-caches ::'{hostname}-git-repositories-{now}' /srv/git/repositories/ 2>&1 | grep -v "Compacting segments"
borg prune --prefix '{hostname}-' --keep-daily 7 --keep-weekly 4 --keep-monthly 6 2>&1 | grep -v "Compacting segments"

# Backup Gitlab Artifacts
export BORG_REPO="$BORG_SERVER/./borg-backups/gitlab-artifacts"
borg create --compression lzma,3 --exclude-caches ::'{hostname}-gitlab-artifacts-{now}' /srv/git/shared/ 2>&1 | grep -v "Compacting segments"
borg prune --prefix '{hostname}-' --keep-daily 7 --keep-weekly 4 --keep-monthly 6 2>&1 | grep -v "Compacting segments"

# Backup Gitlab Postgres DB
export BORG_REPO="$BORG_SERVER/./borg-backups/postgres-databases"
sudo -u git -H sh -c "cd /home/git/ && pg_dump gitlab_production" | borg create --compression lzma,3 "::{hostname}-gitlab_production-{now}" - 2>&1 | grep -v "Compacting segments"
borg prune --prefix '{hostname}-' --keep-daily 7 --keep-weekly 4 --keep-monthly 6 2>&1 | grep -v "Compacting segments"

