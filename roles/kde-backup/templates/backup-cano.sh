LOCATION=/root/{{backup_directory}}

# Cleanup old backups
find $LOCATION -name '*gz' -mtime +7 | xargs rm -f

# Backup system configuration, crontabs and package listing
tar -czf $LOCATION/etc.`date +%w`.tgz -C / etc/
tar -czf $LOCATION/crontabs.`date +%w`.tgz -C / var/spool/cron/
dpkg -l > $LOCATION/dpkg.`date +%w`

# Secure the backups
chmod -R 700 $LOCATION

# Transfer them to their final home
cd $LOCATION/..
rsync --timeout=600 --delete -a {{backup_directory}} -4 {{gohma_backup_user}}@micrea.kde.org:
