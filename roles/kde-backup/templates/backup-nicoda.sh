LOCATION=/root/{{backup_directory}}

# Backup databases
for DATABASE in `mysqlshow | grep -v "information_schema" | grep -v "performance_schema" | grep -v "mysql" | tail -n+4 | awk '{print $2}'`; do
    mysqldump --opt --quick --single-transaction --skip-extended-insert  --events --create-options --set-charset  $DATABASE > $LOCATION/db.$DATABASE.`date +%w`.sql
    xz -f $LOCATION/db.$DATABASE.`date +%w`.sql
done

# Backup homes as well
tar -cJf $LOCATION/homes.`date +%w`.tar.xz -C / home/ 2>&1 | grep -v "we read it"

# Backup configuration and list of installed packages
tar -czf $LOCATION/etc.`date +%w`.tgz -C / etc/
tar -czf $LOCATION/crontabs.`date +%w`.tgz -C / var/spool/cron/
dpkg -l > $LOCATION/dpkg.`date +%w`

# Transfer the generated backups
lftp -f ~/bin/backup-options

# Borg backup time! Let's get ready to go...
export BORG_PASSPHRASE='{{backup_borg_passphrase}}'

{% set backup_creds = hetzner_backup_creds[hetzner_backup_host] %}
BORG_SERVER="ssh://{{backup_creds.username}}@{{backup_creds.hostname}}:23"

# Backup /srv
export BORG_REPO="$BORG_SERVER/./borg-backups/nicoda-sites"
borg create --compression zlib,5 --exclude-caches ::'{hostname}-srv-{now}' /srv/ 2>&1 | grep -v "Compacting segments"
borg prune --prefix '{hostname}-' --keep-daily 7 --keep-weekly 4 --keep-monthly 3 2>&1 | grep -v "Compacting segments"
