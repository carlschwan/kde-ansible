#!/bin/bash
LOCATION=/root/{{backup_directory}}

# Backup configuration and list of installed packages
tar -czf $LOCATION/etc.`date +%w`.tgz -C / etc/
tar -czf $LOCATION/crontabs.`date +%w`.tgz -C / var/spool/cron/
dpkg -l > $LOCATION/dpkg.`date +%w`

# Grab Grafana's own backups
cp /var/lib/grafana/grafana.db $LOCATION/grafana.db.`date +%w`

# Transfer backups to Gohma
chmod -R 700 $LOCATION
rsync --timeout=600 -a {{backup_directory}} {{gohma_backup_user}}@micrea.kde.org:

# Prepare to run Borg
export BORG_PASSPHRASE='{{backup_borg_passphrase}}'
export BORG_SERVER="{{gohma_backup_user}}@micrea.kde.org"

# Backup InfluxDB for Grafana
export BORG_REPO="$BORG_SERVER:{{backup_directory}}/borg/influxdb-grafana"
borg create --compression zlib,3 --exclude-caches ::'{hostname}-influxdb-{now}' /var/lib/influxdb 2>&1 | grep -v "Compacting segments"
borg prune --prefix '{hostname}-' --keep-daily 7 --keep-weekly 4 --keep-monthly 6 2>&1 | grep -v "Compacting segments"
