#!/bin/bash
LOCATION=/root/{{backup_directory}}

# Backup configuration and list of installed packages
tar -czf $LOCATION/etc.`date +%w`.tgz -C / etc/
tar -czf $LOCATION/crontabs.`date +%w`.tgz -C / var/spool/cron/
dpkg -l > $LOCATION/dpkg.`date +%w`

lftp -f ~/bin/backup-options

# Borg backup time! Let's get ready to go...
export BORG_PASSPHRASE='{{backup_borg_passphrase}}'

{% set backup_creds = hetzner_backup_creds[hetzner_backup_host] %}
BORG_SERVER="ssh://{{backup_creds.username}}@{{backup_creds.hostname}}:23"

# Backup databases
export BORG_REPO="$BORG_SERVER/./borg-backups/mimi-databases"
for DATABASE in `mysqlshow | grep -v "information_schema" | grep -v "performance_schema" | grep -v "mysql" | tail -n+4 | awk '{print $2}'`; do
    mysqldump --opt --quick --single-transaction --skip-extended-insert  --events --create-options --set-charset  $DATABASE | borg create --compression lzma,3 "::{hostname}-$DATABASE-{now}" - 2>&1 | grep -v "Compacting segments"
    borg prune --prefix "{hostname}-$DATABASE" --keep-daily 7 --keep-weekly 4 --keep-monthly 6 2>&1 | grep -v "Compacting segments"
done

# Backup home directories
export BORG_REPO="$BORG_SERVER/./borg-backups/mimi-homes"
borg create --compression zlib,5 --exclude-caches ::'{hostname}-homes-{now}' /home/ 2>&1 | grep -v "Compacting segments"
borg prune --prefix '{hostname}-' --keep-daily 7 --keep-weekly 4 --keep-monthly 6 2>&1 | grep -v "Compacting segments"

# Backup website contents
export BORG_REPO="$BORG_SERVER/./borg-backups/mimi-app-files"
borg create --compression zlib,5 --exclude-caches --exclude /srv/www/reviewboard/svn.reviewboard.kde.org/logs --exclude /srv/www/reviewboard/git.reviewboard.kde.org/logs --exclude /srv/www/reports.kde.org/log/production.log --exclude /srv/www/reviewboard/svn.reviewboard.kde.org/search-index --exclude /srv/www/reviewboard/git.reviewboard.kde.org/search-index --exclude /srv/www/conf.kde.org/log ::'{hostname}-srv-{now}' /srv/ 2>&1 | grep -v "Compacting segments"
borg prune --prefix '{hostname}-' --keep-daily 7 --keep-weekly 4 --keep-monthly 6 2>&1 | grep -v "Compacting segments"
