#!/usr/bin/python3

# SPDX-FileCopyrightText: 2020 Nicolás Alvarez <nicolas.alvarez@gmail.com>
# SPDX-License-Identifier: MIT

import configparser
import subprocess
import datetime
import sys
from socket import socket, AF_UNIX, SOCK_STREAM

config = configparser.ConfigParser()
config.read("bin/backup-credentials.cfg")

# Test mode disables actually sending data to telegraf
test_mode = False
if len(sys.argv) > 1 and sys.argv[1] == '--test':
    test_mode = True

command = 'connect sftp://{hostname}/ -u {username},{password}; du -b -d2 /'.format(**config['backup'])

print("Getting backup sizes")
proc = subprocess.run(['lftp'], input=command.encode('utf-8'), stdout=subprocess.PIPE)

output = proc.stdout.decode('utf-8').strip()
lines = output.split('\n')
print("{} directories counted".format(len(lines)))

# Get current time with minute resolution
dt = datetime.datetime.utcnow().replace(second=0, microsecond=0)
# InfluxDB expects nanoseconds
timestamp = int(dt.timestamp()*1000*1000*1000)

print("Logging data with timestamp {}".format(dt.isoformat()))

if not test_mode:
    print("Connecting to telegraf socket")
    sock = socket(AF_UNIX, SOCK_STREAM)
    sock.connect('/tmp/telegraf.sock')
else:
    print("Test mode enabled, won't write to telegraf")

try:
    for line in lines:
        strsize, path = line.split('\t')
        size = int(strsize)

        influx_line = 'backup_size,backup_host={},path={} size={:d}i {}\n'.format(config["backup"]["space_name"], path, size, timestamp)
        print(influx_line, end='')
        if not test_mode:
            sock.send(influx_line.encode('utf-8'))
finally:
    if not test_mode:
        sock.close()
