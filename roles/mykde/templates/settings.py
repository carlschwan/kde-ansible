"""
Local development settings.

Copy to settings.py and edit to suit your needs.
"""

# noinspection PyUnresolvedReferences
from mykdeorg.common_settings import *
import sys
import os
import dj_database_url

DEBUG = {{mykde_debug_enabled | bool | ternary('True','False')}}
DISABLE_ACOUNT_CREATION = True
DEFAULT_FROM_EMAIL = 'noreply@kde.org'

# Update this to something unique for your machine.
# This was generated using "pwgen -sync 64 -r '"'"
SECRET_KEY = "{{mykde_secret_key}}"

# For testing purposes, allow HTTP as well as HTTPS. Never enable this in production!
# OAUTH2_PROVIDER['ALLOWED_REDIRECT_URI_SCHEMES'] = ['http', 'https']
DATABASES = {
    'default': dj_database_url.parse('mysql://mykde:{{mysql_mykde_password}}@127.0.0.1:3306/mykde', conn_max_age=600),
}

# Enabled features
ENABLE_DEV_REQUESTS = False


LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'default': {
            'format': '%(asctime)-15s %(levelname)8s %(name)s %(message)s'
        },
        'verbose': {
            'format': '%(asctime)-15s %(levelname)8s %(name)s %(process)d %(thread)d %(message)s'
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'default',  # Set to 'verbose' in production
            'stream': 'ext://sys.stderr',
        },
        # Enable this in production:
        # 'sentry': {
        #     'level': 'ERROR',  # To capture more than ERROR, change to WARNING, INFO, etc.
        #     'class': 'raven.contrib.django.raven_compat.handlers.SentryHandler',
        #     # 'tags': {'custom-tag': 'x'},
        # },
    },
    'loggers': {
        'bid_main': {'level': 'DEBUG'},
        'mykdeorg': {'level': 'DEBUG'},
        'bid_api': {'level': 'DEBUG'},
        'bid_addon_support': {'level': 'DEBUG'},
        'sentry.errors': {'level': 'DEBUG', 'handlers': ['console'], 'propagate': False},
    },
    'root': {
        'level': 'WARNING',
        'handlers': [
            'console',
            # Enable this in production:
            # 'sentry',
        ],
    }
}

# For Debug Toolbar, extend with whatever address you use to connect
# to your dev server.
INTERNAL_IPS = ['127.0.0.1']

ALLOWED_HOSTS = ['{{mykde_hostname}}']

# Don't use this in production, but only in tests.
# ALLOWED_HOSTS = ['*']


# # Raven is the Sentry.io integration app for Django. Enable this on production:
# import os
# import raven
# INSTALLED_APPS.append('raven.contrib.django.raven_compat')
#
# RAVEN_CONFIG = {
#     'dsn': 'https://<key>:<secret>@sentry.io/<project>',
#     # If you are using git, you can also automatically configure the
#     # release based on the git info.
#     'release': raven.fetch_git_sha(os.path.abspath(os.curdir)),
# }

# For development, dump email to the console instead of trying to actually send it.
# EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
# Alternatively, run python3 -m smtpd -n -c DebuggingServer -d '0.0.0.0:2525' and set
# EMAIL_PORT = 2525

EMAIL_HOST = 'localhost'
EMAIL_HOST_USER = "noreply@kde.org"

# Hosts that we allow redirecting to with a next=xxx parameter on the /login and /switch
# endpoints; this is an addition to the defaults, for development purposes.
# NEXT_REDIR_AFTER_LOGIN_ALLOWED_HOSTS.update({
#     'cloud.local:5000', 'cloud.local:5001', 'cloud.local',
# })
