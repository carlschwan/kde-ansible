{% extends "apache-vhost.j2" %}

{% block common %}
    ServerAdmin  webmaster@kde.org
    ServerName   {{maps_hostname}}
    DocumentRoot {{maps_static_tiles_dir}}

    ErrorLog  ${APACHE_LOG_DIR}/maps.kde.org-error.log
    CustomLog ${APACHE_LOG_DIR}/maps.kde.org.log combined
{% endblock %}

{% block redirect %}
    Redirect / https://{{maps_hostname}}/
{% endblock %}

{% block main %}
    Include common/mod_tile.conf

    # Integrate staticly generated low-z tiles and dynamically generated high-z tiles
    # provided by Tirex. For this low-z requests are forwarded 1:1 to the file system,
    # high-z requests end up with Tirex via a rewrite rule.

    # mod_tile assumes file extensions are consisting only of lower-case letters, while we want them to be "o5m"
    # we achieve that by rewriting the file extension to one we use internally for this ("ofm")
    # However, we must only do that for levels 11 to 17 which are delivered via Tirex, levels 1 to 9 are static
    # and delivered differently.

    RewriteEngine on
    RewriteRule ^(.*/vectorosm/v1/\d\d/\d+/\d+)\.o5m$ /tirex$1.ofm [PT]
    RewriteRule ^(.*/vectorosm/dev/\d\d/\d+/\d+)\.o5m$ /tirex$1.ofm [PT]
{%- endblock %}

{% block ssl %}
    # Enable SSL
    Include /etc/apache2/common/ssl.conf
    Include /etc/apache2/common/ssl-star-kde-org.conf
{% endblock %}
